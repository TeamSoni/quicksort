package io.github.soniex2.quicksort.util;

import com.google.common.collect.Iterables;
import io.github.soniex2.quicksort.api.sorter.Algorithm;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Iterator;

/**
 * @author soniex2
 */
@ParametersAreNonnullByDefault
public class AlgorithmUtils {

    /**
     * Find the algorithm that comes after the given algorithm. Useful for cycling through algorithms, such as
     * when requested by user.
     *
     * @param from The given algorithm.
     * @return The next algorithm.
     */
    @Nullable
    public static Algorithm findNext(@Nullable Algorithm from) {
        IForgeRegistry<Algorithm> algorithms = GameRegistry.findRegistry(Algorithm.class);
        Iterator<Algorithm> it = algorithms.iterator();
        if (from == null) {
            return it.hasNext() ? it.next() : null;
        }
        while (it.hasNext()) {
            if (it.next() == from) {
                break;
            }
        }
        return it.hasNext() ? it.next() : Iterables.getFirst(algorithms, null);
    }
}
