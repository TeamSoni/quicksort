package io.github.soniex2.quicksort.util;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

/**
 * @author soniex2
 */
public class NoCapabilityStorage<T> implements Capability.IStorage<T> {
    private NoCapabilityStorage() {

    }

    @Nullable
    @Override
    public NBTBase writeNBT(Capability<T> capability, T instance, EnumFacing side) {
        return null;
    }

    @Override
    public void readNBT(Capability<T> capability, T instance, EnumFacing side, NBTBase nbt) {
    }

    private static final NoCapabilityStorage<?> INSTANCE = new NoCapabilityStorage<>();

    /**
     * Returns an instance of this class. This method is type-safe.
     *
     * @param <T> The capability type.
     * @return An instance of this class.
     */
    @SuppressWarnings("unchecked")
    public static <T> NoCapabilityStorage<T> instance() {
        return (NoCapabilityStorage<T>) INSTANCE;
    }
}
