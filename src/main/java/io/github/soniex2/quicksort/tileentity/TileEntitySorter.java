package io.github.soniex2.quicksort.tileentity;

import io.github.soniex2.quicksort.api.Face;
import io.github.soniex2.quicksort.api.cap.Capabilities;
import io.github.soniex2.quicksort.api.sorter.Algorithm;
import io.github.soniex2.quicksort.api.sorter.ExtendedData;
import io.github.soniex2.quicksort.api.sorter.Sorter;
import io.github.soniex2.quicksort.block.BlockSorter;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

/**
 * @author soniex2
 */
public class TileEntitySorter extends TileEntity implements ITickable {
    // implementing Sorter in the TE just crashes.
    private final SorterImpl sorter = new SorterImpl();

    public boolean hasSorted = false;

    @Override
    public boolean receiveClientEvent(int id, int type) {
        if (id == BlockSorter.SET_SORTED_BLOCKEVENT) {
            hasSorted = type != 0;
            return true;
        }
        return super.receiveClientEvent(id, type);
    }

    @Override
    public void update() {
        if (hasSorted) {
            // this takes a whole tick when called from this method
            world.addBlockEvent(pos, getBlockType(), BlockSorter.SET_SORTED_BLOCKEVENT, 0);
        }
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        if (capability == Capabilities.CAP_SORTER) {
            return true;
        }
        Algorithm a = getAlgorithm();
        if (a != null && a.getCapabilityProvider(sorter).hasCapability(capability, facing)) {
            return true;
        }
        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == Capabilities.CAP_SORTER) {
            return Capabilities.CAP_SORTER.cast(sorter);
        }
        Algorithm a = getAlgorithm();
        if (a != null) {
            T t = a.getCapabilityProvider(sorter).getCapability(capability, facing);
            if (t != null) {
                return t;
            }
        }
        return super.getCapability(capability, facing);
    }

    public void operate() {
        Algorithm algorithm = getAlgorithm();
        if (algorithm == null) {
            return;
        }
        hasSorted = algorithm.operate(sorter);
        world.addBlockEvent(pos, getBlockType(), BlockSorter.SET_SORTED_BLOCKEVENT, hasSorted ? 1 : 0);
    }

    private Algorithm getAlgorithm() {
        // TODO

        //return null;
        throw new UnsupportedOperationException("getAlgorithm");
    }

    /**
     * Sorter impl. Avoids obfuscation issues.
     */
    private class SorterImpl implements Sorter {
        @Override
        public World getWorld() {
            return TileEntitySorter.this.getWorld();
        }

        @Override
        public BlockPos getPos() {
            return TileEntitySorter.this.getPos();
        }

        @Override
        public Face getTarget() {
            // TODO test
            World w = getWorld();
            BlockPos p = getPos();
            EnumFacing f = w.getBlockState(p).getValue(BlockSorter.FACING);
            return new Face(getWorld(), p.offset(f), f.getOpposite());
        }

        @Override
        public Face getSource() {
            // TODO test
            World w = getWorld();
            BlockPos p = getPos();
            EnumFacing f = w.getBlockState(p).getValue(BlockSorter.FACING);
            return new Face(getWorld(), p.offset(f, -1), f);
        }

        @Override
        public ExtendedData getExtendedData() {
            throw new UnsupportedOperationException("getExtendedData");
        }
    }
}
