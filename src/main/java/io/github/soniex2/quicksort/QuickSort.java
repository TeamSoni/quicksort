package io.github.soniex2.quicksort;

import io.github.soniex2.quicksort.api.sorter.Algorithm;
import io.github.soniex2.quicksort.init.QSAlgorithms;
import io.github.soniex2.quicksort.init.QSBlocks;
import io.github.soniex2.quicksort.init.QSCapabilities;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

/**
 * @author soniex2
 */
@Mod(modid = QuickSort.MODID, name = "QuickSort", version = "1.0.0", acceptedMinecraftVersions = "[1.12,1.12.1]")
public class QuickSort {

    @Mod.Instance
    private static QuickSort instance;

    public static final String MODID = "quicksort";
    private Logger log;

    public QuickSort() {
        MinecraftForge.EVENT_BUS.register(this);
        QSCapabilities.register();
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        log = event.getModLog();
    }

    @SubscribeEvent
    public void registerBlocks(RegistryEvent.Register<Block> event) {
        QSBlocks.registerBlocks(event.getRegistry());
    }

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
        QSBlocks.registerItemBlocks(event.getRegistry());
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void registerAlgorithms(RegistryEvent.Register<Algorithm> event) {
        QSAlgorithms.register(event.getRegistry());
    }

    @SubscribeEvent
    public void registerRegistries(RegistryEvent.NewRegistry event) {
        IForgeRegistry<Algorithm> algorithmRegistry = new RegistryBuilder<Algorithm>()
                .setName(new ResourceLocation(MODID, "sorter_algorithms"))
                .setType(Algorithm.class)
                .create();
    }

    public static Optional<Logger> log() {
        if (instance == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(instance.log);
    }
}
