package io.github.soniex2.quicksort.algorithm;

import io.github.soniex2.quicksort.api.sorter.Algorithm;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.*;

import javax.annotation.Nullable;

/**
 * @author soniex2
 */
public abstract class AbstractAlgorithm implements Algorithm {
    private ResourceLocation registryName;

    @Override
    public final Algorithm setRegistryName(ResourceLocation name) {
        if (getRegistryName() != null) {
            throw new IllegalStateException();
        }
        ModContainer mc = Loader.instance().activeModContainer();
        String prefix = mc == null || (mc instanceof InjectedModContainer && ((InjectedModContainer) mc).wrappedContainer instanceof FMLContainer) ? "minecraft" : mc.getModId().toLowerCase();
        if (!name.getResourceDomain().equals(prefix)) {
            FMLLog.bigWarning("Dangerous alternative prefix `{}` for name `{}`, expected `{}` invalid registry invocation/invalid name?", name.getResourceDomain(), name, prefix);
        }
        this.registryName = name;
        return this;
    }

    @Nullable
    @Override
    public final ResourceLocation getRegistryName() {
        return registryName;
    }
}
