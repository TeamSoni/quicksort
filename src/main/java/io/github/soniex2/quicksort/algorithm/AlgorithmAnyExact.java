package io.github.soniex2.quicksort.algorithm;

import io.github.soniex2.quicksort.api.sorter.Sorter;

import javax.annotation.Nonnull;

/**
 * @author soniex2
 */
public class AlgorithmAnyExact extends AbstractAlgorithm {

    @Override
    public boolean operate(@Nonnull Sorter sorter) {
        throw new UnsupportedOperationException("operate");
    }

}
