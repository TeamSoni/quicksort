package io.github.soniex2.quicksort.init;

import io.github.soniex2.quicksort.QuickSort;
import io.github.soniex2.quicksort.block.BlockSorter;
import io.github.soniex2.quicksort.tileentity.TileEntitySorter;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IRegistryDelegate;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author soniex2
 */
@ParametersAreNonnullByDefault
public enum QSBlocks {
    SORTER(shortName("sorter"), BlockSorter::new, TileEntitySorter.class);

    /**
     * 0 if unregistered, 1 if blocks registered, 2 if itemblocks registered.
     */
    private static int registerState;

    private final ResourceLocation registryName;
    private final Supplier<Block> supplier;
    private final Consumer<Block> lazyProps;
    private final Class<? extends TileEntity> tileClass;
    private IRegistryDelegate<Block> delegate;

    QSBlocks(ResourceLocation registryName, Supplier<Block> supplier) {
        this(registryName, supplier, null, o -> {
        });
    }

    QSBlocks(ResourceLocation registryName, Supplier<Block> supplier, Consumer<Block> lazyProps) {
        this(registryName, supplier, null, lazyProps);
    }

    QSBlocks(ResourceLocation registryName, Supplier<Block> supplier, @Nullable Class<? extends TileEntity> tileClass) {
        this(registryName, supplier, tileClass, o -> {
        });
    }

    QSBlocks(ResourceLocation registryName, Supplier<Block> supplier, @Nullable Class<? extends TileEntity> tileClass, Consumer<Block> lazyProps) {
        this.registryName = registryName;
        this.supplier = supplier;
        this.lazyProps = lazyProps;
        this.tileClass = tileClass;
    }

    public Block get() {
        if (registerState < 1) {
            throw new IllegalStateException("Too early!");
        }
        return delegate == null ? Blocks.AIR : delegate.get();
    }

    public static void registerBlocks(IForgeRegistry<Block> registry) {
        for (QSBlocks block : values()) {
            Block b = block.supplier.get().setRegistryName(block.registryName);
            block.lazyProps.accept(b);
            registry.register(b);
            block.delegate = b.delegate;
            if (block.tileClass != null) {
                GameRegistry.registerTileEntity(block.tileClass, block.registryName.toString());
            }
        }
        registerState = 1;
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        for (QSBlocks block : values()) {
            registry.register(new ItemBlock(block.get()).setRegistryName(block.registryName));
        }
        registerState = 2;
    }

    private static ResourceLocation shortName(String name) {
        return new ResourceLocation(QuickSort.MODID, name);
    }
}
