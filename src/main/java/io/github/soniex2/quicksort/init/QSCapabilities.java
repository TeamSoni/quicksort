package io.github.soniex2.quicksort.init;

import com.google.common.collect.Lists;
import io.github.soniex2.quicksort.api.cap.ioitem.CapabilityDyeColor;
import io.github.soniex2.quicksort.api.sorter.Sorter;
import io.github.soniex2.quicksort.util.NoCapabilityStorage;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.function.Function;

/**
 * @author soniex2
 */
public enum QSCapabilities {
    // The sorter is special. No storage and no default instance.
    SORTER(Sorter.class, NoCapabilityStorage.instance(), () -> null),
    DYE_COLOR(CapabilityDyeColor.class,
            (capability, instance, side) -> new NBTTagString(instance.getDyeColor().getName().toLowerCase(Locale.ROOT)),
            (Capability<CapabilityDyeColor> capability, CapabilityDyeColor instance, EnumFacing side, NBTBase nbt) -> {
                if (nbt.getId() != Constants.NBT.TAG_STRING) {
                    return;
                }
                String s = ((NBTTagString) nbt).getString().toLowerCase(Locale.ROOT);
                for (EnumDyeColor dyeColor : EnumDyeColor.values()) {
                    if (dyeColor.getName().toLowerCase(Locale.ROOT).equals(s)) {
                        instance.setDyeColor(dyeColor);
                        return;
                    }
                }
                throw new RuntimeException("???");
            }, () -> new CapabilityDyeColor(EnumDyeColor.WHITE));

    private CapHolder<?> capHolder;

    <T> QSCapabilities(Class<T> type, Capability.IStorage<T> storage, Callable<T> factory) {
        capHolder = new CapHolder<>(type, storage, factory);
    }

    <T> QSCapabilities(Class<T> type, WriteNBT<T> writeNBT, ReadNBT<T> readNBT, Callable<T> factory) {
        capHolder = new CapHolder<>(type, new Capability.IStorage<T>() {
            @Nullable
            @Override
            public NBTBase writeNBT(Capability<T> capability, T instance, EnumFacing side) {
                return writeNBT.writeNBT(capability, instance, side);
            }

            @Override
            public void readNBT(Capability<T> capability, T instance, EnumFacing side, NBTBase nbt) {
                readNBT.readNBT(capability, instance, side, nbt);
            }
        }, factory);
    }

    public static void register() {
        for (QSCapabilities val : values()) {
            val.capHolder.register();
        }
    }

    public Capability<?> get() {
        return capHolder.capability;
    }

    private static class CapHolder<T> {
        private final Class<T> type;
        private final Capability.IStorage<T> storage;
        private final Callable<T> factory;
        private Capability<T> capability;

        private static final Field REFLECTING = ReflectionHelper.findField(CapabilityManager.class, "callbacks");

        CapHolder(Class<T> type, Capability.IStorage<T> storage, Callable<T> factory) {
            this.type = type;
            this.storage = storage;
            this.factory = factory;
        }

        Capability<T> getCapability() {
            return capability;
        }

        void register() {
            CapabilityManager.INSTANCE.register(type, storage, factory);
        }

        @SuppressWarnings("unchecked")
        void reflectIntoCapabilityManager() {
            // Why couldn't forge just have an official way of doing this?
            // Why do I have to go through so much pain?
            // #FuckForge
            try {
                IdentityHashMap<String, List<Function<Capability<?>, Object>>> map = (IdentityHashMap<String, List<Function<Capability<?>, Object>>>) REFLECTING.get(CapabilityManager.INSTANCE);
                List<Function<Capability<?>, Object>> list = map.computeIfAbsent(type.getName().replace('.', '/'), k -> Lists.newArrayList());
                list.add(cap -> {
                    capability = (Capability<T>) cap;
                    return null;
                });
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Ah, fuck forge.", e);
            }
        }
    }

    @FunctionalInterface
    private static interface WriteNBT<T> {
        @Nullable
        public NBTBase writeNBT(Capability<T> capability, T instance, EnumFacing side);
    }

    @FunctionalInterface
    private static interface ReadNBT<T> {
        public void readNBT(Capability<T> capability, T instance, EnumFacing side, NBTBase nbt);
    }
}
