package io.github.soniex2.quicksort.init;

import io.github.soniex2.quicksort.QuickSort;
import io.github.soniex2.quicksort.algorithm.AlgorithmAnyExact;
import io.github.soniex2.quicksort.api.sorter.Algorithm;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistry;

import java.util.function.Supplier;

/**
 * @author soniex2
 */
public enum QSAlgorithms {
    ANY_EXACT(shortName("any_exact"), AlgorithmAnyExact::new);

    private final ResourceLocation registryName;
    private final Supplier<Algorithm> supplier;

    QSAlgorithms(ResourceLocation registryName, Supplier<Algorithm> supplier) {
        this.registryName = registryName;
        this.supplier = supplier;
    }

    public static void register(IForgeRegistry<Algorithm> algorithmRegistry) {
        for (QSAlgorithms algorithm : values()) {
            algorithmRegistry.register(algorithm.supplier.get().setRegistryName(algorithm.registryName));
        }
    }

    private static ResourceLocation shortName(String name) {
        return new ResourceLocation(QuickSort.MODID, name);
    }
}
