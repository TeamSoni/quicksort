package io.github.soniex2.quicksort.api.sorter;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

/**
 * Represents an item going into the output of the sorter.
 *
 * @author soniex2
 */
public interface OutputItem extends ICapabilityProvider {
    ItemStack getItem();
}
