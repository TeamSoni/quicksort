package io.github.soniex2.quicksort.api;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author soniex2
 */
public class Face extends Node {
    private final EnumFacing side;

    public Face(@Nonnull World world, @Nonnull BlockPos pos, @Nullable EnumFacing side) {
        super(world, pos);
        this.side = side;
    }

    public EnumFacing getSide() {
        return side;
    }

    public boolean hasCapability(@Nonnull Capability<?> capability) {
        return super.hasCapability(capability, side);
    }

    @Nullable
    public <T> T getCapability(@Nonnull Capability<T> capability) {
        return super.getCapability(capability, side);
    }
}
