package io.github.soniex2.quicksort.api.sorter;

import com.google.common.collect.Maps;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * A very nice implementation for extended data.
 *
 * @author soniex2
 */
@NotThreadSafe
public final class ExtendedData implements INBTSerializable<NBTTagCompound> {
    private Map<ResourceLocation, INBTSerializable<?>> dataMap = Maps.newHashMap();
    private Map<ResourceLocation, NBTBase> nbtMap = Maps.newHashMap();

    @Override
    public NBTTagCompound serializeNBT() {
        final NBTTagCompound out = new NBTTagCompound();
        dataMap.forEach((k, v) -> {
            try {
                out.setTag(k.toString(), v.serializeNBT());
            } catch (StackOverflowError e) {
                // This helps with debugging.
                StackOverflowError e2 = new StackOverflowError("Stack overflow in ExtendedData serialization. Key: " + k);
                e2.initCause(e);
                throw e2;
            }
        });
        nbtMap.forEach((k, v) -> out.setTag(k.toString(), v));
        return out;
    }

    @Override
    public void deserializeNBT(NBTTagCompound in) {
        in.getKeySet().forEach(k -> nbtMap.put(new ResourceLocation(k), in.getTag(k)));
        dataMap.clear();
    }

    /**
     * Associates the given data with the given ID. If there's already a mapping for the given ID, it is replaced.
     *
     * @param id   The ID.
     * @param data The data.
     */
    public void put(@Nonnull ResourceLocation id, @Nonnull INBTSerializable<?> data) {
        dataMap.put(id, data);
        nbtMap.remove(id);
    }

    /**
     * Removes the mapping for the given ID.
     *
     * @param id The ID.
     */
    public void remove(@Nonnull ResourceLocation id) {
        dataMap.remove(id);
        nbtMap.remove(id);
    }

    /**
     * Retrieves the value associated with the given ID, regardless of its type. Does not deserialize data!
     *
     * @param id The ID.
     * @return The value associated with the given ID, if it's already deserialized. {@code null} otherwise.
     */
    public INBTSerializable<?> rawGet(@Nonnull ResourceLocation id) {
        return dataMap.get(id);
    }

    /**
     * Retrieves the value associated with the given ID, deserializing as needed.
     *
     * @param id       The ID.
     * @param dataType The type of the data.
     * @param nbtType  The type of the NBT.
     * @param <T>      The NBT type.
     * @param <U>      The data type.
     * @return The value associated with the given ID deserialized to the given type, if possible. {@code null} otherwise.
     */
    @Nullable
    public <T extends NBTBase, U extends INBTSerializable<? super T>> U get(@Nonnull ResourceLocation id, @Nonnull Class<U> dataType, @Nonnull Class<T> nbtType) {
        INBTSerializable<?> data = dataMap.get(id);
        NBTBase nbt = nbtMap.get(id);
        U instance = dataType.isInstance(data) ? dataType.cast(data) : null;
        if (instance == null && nbt != null) {
            if (!nbtType.isInstance(nbt)) {
                return null;
            }
            try {
                Constructor<U> constructor = dataType.getDeclaredConstructor();
                constructor.setAccessible(true);
                instance = constructor.newInstance();
            } catch (InstantiationException e) {
                throw new IllegalArgumentException("Class is abstract", e);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            } catch (NoSuchMethodException e) {
                throw new IllegalArgumentException("Class needs to have no-args constructor", e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }
            instance.deserializeNBT(nbtType.cast(nbt));
            dataMap.put(id, instance);
            nbtMap.remove(id);
        }
        return instance;
    }
}
