package io.github.soniex2.quicksort.api.sorter;

import io.github.soniex2.quicksort.api.Face;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;

/**
 * @author soniex2
 */
public interface Sorter {
    /**
     * @return The sorter's World.
     */
    World getWorld();

    /**
     * @return The sorter's BlockPos.
     */
    BlockPos getPos();

    Face getTarget();

    Face getSource();

    @Nonnull
    ExtendedData getExtendedData();

    /**
     * Retrieves this sorter's suggested filter inventory width.
     *
     * @return This sorter's suggested filter inventory width.
     * @see #getDefaultClientGuiComponent()
     * @see #getDefaultServerGuiComponent()
     */
    int getWidth();

    /**
     * Retrieves this sorter's suggested filter inventory height.
     *
     * @return This sorter's suggested filter inventory height.
     * @see #getDefaultClientGuiComponent()
     * @see #getDefaultServerGuiComponent()
     */
    int getHeight();

    /**
     * The default Container used by this sorter.
     *
     * @return A Container which uses the sorter's {@link #getWidth()} and {@link #getHeight()}, as well as
     * an algorithm's {@link Algorithm#getCapabilityProvider(Sorter)}.
     * @see #getDefaultClientGuiComponent()
     */
    @Nonnull
    Container getDefaultServerGuiComponent();

    /**
     * The default GuiContainer used by this sorter.
     *
     * @return A GuiContainer which uses the sorter's {@link #getWidth()} and {@link #getHeight()}, as well as
     * an algorithm's {@link Algorithm#getServerGuiComponent(Sorter)}.
     * @see #getDefaultServerGuiComponent()
     */
    @Nonnull
    @SideOnly(Side.SERVER)
    GuiContainer getDefaultClientGuiComponent();
}
