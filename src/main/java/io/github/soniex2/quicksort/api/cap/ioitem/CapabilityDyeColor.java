package io.github.soniex2.quicksort.api.cap.ioitem;

import net.minecraft.item.EnumDyeColor;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A capability for representing dye colors.
 *
 * @author soniex2
 */
@ThreadSafe
public final class CapabilityDyeColor {
    @Nonnull
    private AtomicReference<EnumDyeColor> dyeColor = new AtomicReference<>();

    /**
     * Construct a new dye color capability with the given color.
     *
     * @param dyeColor The dye color.
     */
    public CapabilityDyeColor(@Nonnull EnumDyeColor dyeColor) {
        this.dyeColor.set(dyeColor);
    }

    /**
     * Returns the dye color represented by this capability.
     *
     * @return The dye color.
     */
    @Nonnull
    public EnumDyeColor getDyeColor() {
        return dyeColor.get();
    }

    /**
     * Sets a new dye color.
     *
     * @param dyeColor The dye color.
     */
    public void setDyeColor(@Nonnull EnumDyeColor dyeColor) {
        this.dyeColor.set(dyeColor);
    }

    /**
     * Returns the AtomicReference powering this class. Using this method may cause hard-to-trace NPEs.
     *
     * @return The AtomicReference powering this class.
     */
    @Nonnull
    public AtomicReference<EnumDyeColor> getRef() {
        return dyeColor;
    }
}
