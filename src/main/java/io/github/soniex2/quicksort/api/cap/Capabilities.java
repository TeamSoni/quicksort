package io.github.soniex2.quicksort.api.cap;

import io.github.soniex2.quicksort.api.cap.ioitem.CapabilityDyeColor;
import io.github.soniex2.quicksort.api.sorter.Sorter;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

/**
 * @author soniex2
 */
public class Capabilities {
    @CapabilityInject(CapabilityDyeColor.class)
    public static final Capability<CapabilityDyeColor> CAP_DYE_COLOR = getCap();

    @CapabilityInject(Sorter.class)
    public static final Capability<Sorter> CAP_SORTER = getCap();

    // Constant folding hack.
    private static <T> Capability<T> getCap() {
        return null;
    }
}
