package io.github.soniex2.quicksort.api.sorter;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

/**
 * Represents an item coming from the input of the sorter.
 *
 * @author soniex2
 */
public interface InputItem extends ICapabilityProvider {
    ItemStack getItem();
}
