package io.github.soniex2.quicksort.api;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * @author soniex2
 */
public class Node implements ICapabilityProvider {
    @Nonnull
    private final World world;
    @Nonnull
    private final BlockPos pos;

    public Node(@Nonnull World world, @Nonnull BlockPos pos) {
        this.world = world;
        this.pos = pos.toImmutable();
    }

    @Nonnull
    public World getWorld() {
        return world;
    }

    @Nonnull
    public BlockPos getPos() {
        return pos;
    }

    @Nonnull
    public IBlockState getBlockState() {
        return world.getBlockState(pos);
    }

    @Nonnull
    public <T extends TileEntity> Optional<T> getExistingTileEntity(@Nonnull Class<T> clazz) {
        if (!world.isBlockLoaded(pos)) {
            return Optional.empty();
        }
        TileEntity te = world.getChunkFromBlockCoords(pos).getTileEntity(pos, Chunk.EnumCreateEntityType.CHECK);
        if (!clazz.isInstance(te)) {
            return Optional.empty();
        }
        return Optional.of(clazz.cast(te));
    }

    @Nonnull
    public <T extends TileEntity> Optional<T> getTileEntity(@Nonnull Class<T> clazz) {
        TileEntity te = world.getTileEntity(pos);
        if (!clazz.isInstance(te)) {
            return Optional.empty();
        }
        return Optional.of(clazz.cast(te));
    }


    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
        return getTileEntity(TileEntity.class).filter(te -> te.hasCapability(capability, facing)).isPresent();
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
        return getTileEntity(TileEntity.class).map(te -> te.getCapability(capability, facing)).orElse(null);
    }
}
