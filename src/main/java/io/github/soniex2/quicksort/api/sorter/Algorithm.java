package io.github.soniex2.quicksort.api.sorter;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.registries.IForgeRegistryEntry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A sorter algorithm.
 *
 * @author soniex2
 */
public interface Algorithm extends IForgeRegistryEntry<Algorithm> {

    /**
     * Operate on a sorter.
     *
     * @param sorter The sorter.
     * @return Whether to indicate a successful operation. Standard sorter changes texture based on this value.
     */
    boolean operate(@Nonnull Sorter sorter);

    /**
     * Called when the sorter's algorithm is about to change.
     *
     * @param sorter The sorter.
     */
    default void switchAlgorithm(@Nonnull Sorter sorter) {
    }

    /**
     * Returns an ICapabilityProvider for the given sorter.
     *
     * @param sorter The sorter.
     * @return An ICapabilityProvider for the sorter.
     */
    @Nonnull
    default ICapabilityProvider getCapabilityProvider(@Nonnull Sorter sorter) {
        return new DefaultCapabilityProvider(sorter);
    }

    @Nonnull
    default Container getServerGuiComponent(@Nonnull Sorter sorter) {
        return sorter.getDefaultServerGuiComponent();
    }

    @Nonnull
    default GuiContainer getClientGuiComponent(@Nonnull Sorter sorter) {
        return sorter.getDefaultClientGuiComponent();
    }

    @Override
    default Class<Algorithm> getRegistryType() {
        return Algorithm.class;
    }

    /**
     * The ICapabilityProvider returned from {@link #getCapabilityProvider(Sorter)} by default.
     */
    class DefaultCapabilityProvider implements ICapabilityProvider {
        private final Sorter sorter;

        /**
         * Constructs a new DefaultCapabilityProvider for the given sorter.
         * @param sorter The sorter.
         */
        public DefaultCapabilityProvider(Sorter sorter) {
            this.sorter = sorter;
        }

        @Override
        public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
            return facing == null && capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
        }

        @Nullable
        @Override
        public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
            if (facing == null && capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
                ExtendedData ed = sorter.getExtendedData();
                ItemStackHandler itemHandler = ed.get(new ResourceLocation("quicksort:filter"), ItemStackHandler.class, NBTTagCompound.class);
                if (itemHandler == null) {
                    ed.put(new ResourceLocation("quicksort:filter"), itemHandler = new ItemStackHandler(sorter.getWidth() * sorter.getHeight()));
                }
                return (T) itemHandler;
            }
            return null;
        }
    }
}
