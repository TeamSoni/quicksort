package io.github.soniex2.quicksort.block;

import io.github.soniex2.quicksort.api.Node;
import io.github.soniex2.quicksort.tileentity.TileEntitySorter;
import net.minecraft.block.BlockDispenser;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

/**
 * @author soniex2
 */
public class BlockSorter extends BlockDispenser {
    public static final int SET_SORTED_BLOCKEVENT = 0;
    public static final PropertyBool SORTED = PropertyBool.create("sorted");

    public BlockSorter() {
        this.setDefaultState(this.blockState.getBaseState().withProperty(SORTED, Boolean.FALSE).withProperty(FACING, EnumFacing.NORTH).withProperty(TRIGGERED, Boolean.FALSE));
    }

    @SuppressWarnings("deprecation")
    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        return getActualStateInternal(state, worldIn, pos);
    }

    @Override
    protected void dispense(World worldIn, BlockPos pos) {
        new Node(worldIn, pos).getTileEntity(TileEntitySorter.class).ifPresent(te -> {
            if (!te.hasSorted) {
                te.operate();
            } else {
                worldIn.addBlockEvent(pos, this, SET_SORTED_BLOCKEVENT, 0);
            }
        });
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, FACING, TRIGGERED, SORTED);
    }

    private IBlockState getActualStateInternal(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        TileEntity te;
        if (worldIn instanceof World) {
            te = ((World) worldIn).getChunkFromBlockCoords(pos).getTileEntity(pos, Chunk.EnumCreateEntityType.CHECK);
        } else {
            te = worldIn.getTileEntity(pos);
        }
        if (te instanceof TileEntitySorter) {
            return state.withProperty(SORTED, ((TileEntitySorter) te).hasSorted);
        }
        return state;
    }
}
